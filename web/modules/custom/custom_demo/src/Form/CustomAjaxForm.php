<?php
/**
 * @file
 * CustomAjaxForm class implementation.
 *
 * Provide AJAX form with validation examples.
 */

namespace Drupal\custom_demo\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CustomAjaxForm.
 */
class CustomAjaxForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_ajax_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('This demo form with AJAX validation.'),
    ];

    $form['system_messages'] = [
      '#markup' => '<div id="form-system-messages"></div>',
      '#weight' => -100,
    ];

    $form['recipient'] = [
      '#type' => 'email',
      '#required' => TRUE,
      '#title' => $this->t('Email'),
      '#weight' => '0',
      '#description' => '11',
      '#ajax' => [
        'callback' => '::validateEmailAjax',
        'event' => 'change',
        'progress' => array(
          'type' => 'throbber',
          'message' => t('Verifying email..'),
        ),
      ],
      // Where to put validation message if any.
      '#suffix' => '<div class="email-validation-message"></div>',
    ];

    $form['subject'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Subject'),
      '#description' => $this->t('Minimum 10 symbols required.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];

    $form['body'] = [
      '#type' => 'textarea',
      '#required' => TRUE,
      '#title' => $this->t('Body'),
      '#description' => $this->t('Minimum 50 symbols required.'),
      '#maxlength' => 255,
      '#weight' => '0',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#ajax' => [
        'callback' => '::ajaxSubmitCallback',
        'event' => 'click',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Validate email.
    if (substr($form_state->getValue('recipient'), -11) == 'example.com') {
      $form_state->setError($form['recipient'], $this->t('This provider can lost our mail. Be careful!'));
    }

    // Validate subject.
    if (strlen($form_state->getValue('subject')) < 10) {
      $form_state->setError($form['subject'], $this->t('The title must be at least 10 characters long.'));
    }

    // Validate body.
    if (strlen($form_state->getValue('body')) < 50) {
      $form_state->setError($form['body'], $this->t('The title must be at least 50 characters long.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Display results.
    $renderable = [
      '#theme' => 'custom_demo_success_message',
      '#recipient' => $form_state->getValue('recipient'),
      '#subject' => $form_state->getValue('subject'),
      '#body' => $form_state->getValue('body'),
    ];

    $rendered = \Drupal::service('renderer')->render($renderable);
    $this->messenger()->addMessage($rendered);

  }

  /**
   * Email field AJAX validation callback.
   */
  public function validateEmailAjax(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    $message = $form_state->getError($form['recipient']);
    $this->messenger()->addMessage($message);
    if (!empty($message)) {
      $color = 'red';
    }
    elseif (substr($form_state->getValue('recipient'), -11) == 'example.com') {
      $message = $this->t('This provider can lost our mail. Be careful!');
      $color = 'red';
    }
    else {
      $message = '';
      $color = 'green';
    }

    // Display error message.
    $response->addCommand(new HtmlCommand('.email-validation-message', $message));

    // Add a command, InvokeCommand, which allows for custom jQuery commands.
    // In this case, we alter the color of the description.
    $response->addCommand(new InvokeCommand('.email-validation-message', 'css', array('color', $color)));

    // Return the AjaxResponse Object.
    return $response;
  }

  /**
   * AJAX form submit callback.
   */
  public function ajaxSubmitCallback(array &$form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();

    $message = [
      '#theme' => 'status_messages',
      '#message_list' => $this->messenger()->deleteAll(),
      '#status_headings' => [
        'status' => t('Status message'),
        'error' => t('Error message'),
        'warning' => t('Warning message'),
      ],
    ];
    $messages = \Drupal::service('renderer')->render($message);

    $ajax_response->addCommand(new HtmlCommand('#form-system-messages', $messages));
    return $ajax_response;
  }

}
