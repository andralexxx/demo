<?php
/**
 * @file
 * CustomConfigForm class implementation.
 *
 * Provide simple form with one autocomplete field that store values in config.
 */

namespace Drupal\custom_demo\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Class CustomConfigForm.
 */
class CustomConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'custom_demo.customconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('custom_demo.customconfig');

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Example shows a single autocomplete element and a submit button'),
    ];

    $entity_id = $config->get('autocomplete');

    $form['autocomplete'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Node title'),
      '#description' => $this->t('Start typing of node title.'),
      '#default_value' => Node::load($entity_id),
      '#target_type' => 'node',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Store values to config.
    $this->config('custom_demo.customconfig')
      ->set('autocomplete', $form_state->getValue('autocomplete'))
      ->save();
  }

}
